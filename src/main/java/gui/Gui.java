package gui;

import model.Person;

import com.vaadin.data.util.BeanContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.*;

import dao.PersonDao;

public class Gui extends UI {
    private static final long serialVersionUID = 1L;

    private PersonDao dao = new PersonDao();

    private Table table = new Table();

    private BeanContainer<Long, Person> beanContainer
        = createBeanContainer();

    private TextField firstName = new TextField("Eesnimi");
    private TextField surname = new TextField("Perekonnanimi");
    private Button addButton = new Button("Lisa");
    private Button deleteButton = new Button("Kustuta");
    private Label label = new Label();

    @Override
    protected void init(VaadinRequest request) {

        VerticalLayout v1 = new VerticalLayout(
                firstName, surname, addButton);

        VerticalLayout v2 = new VerticalLayout(
                deleteButton, label);

        HorizontalLayout horizontalLayout =
                new HorizontalLayout(v1, table, v2);

        initTable();

        addButton.addClickListener(new ClickListener() {
            private static final long serialVersionUID = 1L;

            @Override
            public void buttonClick(ClickEvent event) {

                dao.store(new Person(firstName.getValue(), surname.getValue()));

                firstName.setValue("");
                surname.setValue("");

                refreshLoanList();
            }
        });

        deleteButton.addClickListener(new ClickListener() {
            private static final long serialVersionUID = 1L;

            @Override
            public void buttonClick(ClickEvent event) {

                Long personId = (Long) table.getValue();
                if (personId != null) {
                    dao.delete(personId);
                }

                refreshLoanList();
            }
        });


        table.addItemClickListener(new ItemClickListener() {
            private static final long serialVersionUID = 1L;

            @Override
            public void itemClick(ItemClickEvent event) {
                Object itemId = event.getItemId();


                label.setValue(dao.findById(
                        Long.parseLong(itemId.toString())).toString());

            }

        });

        refreshLoanList();

        setContent(horizontalLayout);
    }

    private void refreshLoanList() {
        beanContainer.removeAllItems();
        beanContainer.addAll(dao.findAll());
    }

    private void initTable() {
        table.setContainerDataSource(beanContainer);
        table.setVisibleColumns("firstName", "surname");
        table.setColumnHeaders("Eesnimi", "Perekonnanimi");
        table.setSelectable(true);
    }

    private BeanContainer<Long, Person> createBeanContainer() {
        BeanContainer<Long, Person> container
                = new BeanContainer<Long, Person>(Person.class);

        container.setBeanIdProperty("id");

        return container;
    }

}
