package dao;

import java.util.*;

import model.Person;

public class PersonDao {

    private Long counter = 1L;

    private List<Person> personStore = new ArrayList<Person>();

    public PersonDao() {
        personStore.add(new Person(counter++, "Tiit", "Kask"));
        personStore.add(new Person(counter++, "Mari", "Lepp"));
        personStore.add(new Person(counter++, "Teet", "Kuusk"));
    }

    public void store(Person person) {
        if (person.getId() != null) {
            delete(person.getId());
        } else {
            person.setId(counter++);
        }
        personStore.add(person);
    }

    public List<Person> findAll() {
        return new ArrayList<Person>(personStore);
    }

    public void delete(Long personId) {
        List<Person> remaining = new ArrayList<Person>();
        for (Person person : personStore) {
            if (!person.getId().equals(personId)) {
                remaining.add(person);
            }
        }
        personStore = remaining;
    }

    public Person findById(Long personId) {
        for (Person person : personStore) {
            if (person.getId().equals(personId)) {
                return person;
            }
        }

        throw new IllegalStateException("can't find: " + personId);
    }

}
